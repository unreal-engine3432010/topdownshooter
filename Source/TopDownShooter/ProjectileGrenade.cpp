// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileGrenade.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/ProjectileMovementComponent.h"
//#include "Perception/AISense_Damage.h"

void AProjectileGrenade::ImpactProjectile_OnServer_Implementation()
{
	if (ExplodeTimer == 0)
	{
		ExplodeTimer = ProjectileSetting.ExplodeTime;
		TimerEnabled = true;
	}
	

}



void AProjectileGrenade::ExplodeTick(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (ExplodeTimer > 0)
		{
			ExplodeTimer -= DeltaTime;
		}
		else
		{
			Explode();
		}
	}
}

void AProjectileGrenade::Explode()
{
	TimerEnabled = false;
	if (ProjectileSetting.ExplodeFX)
	{
		SpawnExplodeFX_Multicast(ProjectileSetting.ExplodeFX);
	}
	if (ProjectileSetting.ExplodeSound)
	{
		SpawnExplodeSound_Multicast(ProjectileSetting.ExplodeSound);
	}

	if (ShowDebug)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage / 2.0f, 8, FColor::Red, false, 4.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 8, FColor::Green, false, 4.0f);
	}

	TArray<AActor*> IgnoredActor;
	
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExplodeMaxDamage,
		ProjectileSetting.ExplodeMaxDamage * 0.2f,
		GetActorLocation(),
		ProjectileSetting.ProjectileMaxRadiusDamage / 2.0f,
		ProjectileSetting.ProjectileMaxRadiusDamage,
		ProjectileSetting.DamageFalloff,
		UDamageType::StaticClass(), IgnoredActor, this, GetInstigatorController());

	//UAISense_Damage::ReportDamageEvent(GetWorld(), Hit.GetActor(), GetInstigator(), ProjectileSetting.ProjectileDamage, Hit.Location, Hit.Location);

	this->Destroy();
}

void AProjectileGrenade::SpawnExplodeSound_Multicast_Implementation(USoundBase* ExplodeSound)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExplodeSound, GetActorLocation());
}

void AProjectileGrenade::SpawnExplodeFX_Multicast_Implementation(UParticleSystem* ExplodeFX)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplodeFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
}

void AProjectileGrenade::ImpactProjectile()
{
	ImpactProjectile_OnServer();
}

void AProjectileGrenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	ExplodeTick(DeltaTime);
}

AProjectileGrenade::AProjectileGrenade()
{
	BulletProjectileMovement->InitialSpeed = 1000;
	BulletProjectileMovement->ProjectileGravityScale = 1;
	BulletProjectileMovement->bShouldBounce = true;
	
}
