// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"
#include "ProjectileGrenade.h"
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TopDownShooterCharacter.h"
#include "CharacterHealthComponent.h"
#include "Perception/AISense_Damage.h"


void AProjectile::InitProjectileTrailFX_Multicast_Implementation(UParticleSystem* TrailFX)
{
	BulletFX->SetTemplate(TrailFX);
}

void AProjectile::SpawnProjectileHitSound_Multicast_Implementation(USoundBase* HitSound, FHitResult HitResult)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, HitResult.ImpactPoint);
}

void AProjectile::SpawnProjectileHitDecal_Multicast_Implementation(UMaterialInterface* Decal, UPrimitiveComponent* OtherComp, FHitResult HitResult)
{
	UGameplayStatics::SpawnDecalAttached(Decal, FVector(20.0f), OtherComp, NAME_None, HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
}

void AProjectile::InitProjectileSpeed_Multicast_Implementation(float InitSpeed, float MaxSpeed)
{
	BulletProjectileMovement->Velocity = GetActorForwardVector() * InitSpeed;
	BulletProjectileMovement->InitialSpeed = InitSpeed;
	BulletProjectileMovement->MaxSpeed = MaxSpeed;
}

void AProjectile::PostNetReceiveVelocity(const FVector& NewVelocity)
{
	BulletProjectileMovement->Velocity = NewVelocity;
}

// Sets default values
AProjectile::AProjectile()
{
	bReplicates = true;

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));

	BulletCollisionSphere->SetSphereRadius(16.f);

	BulletCollisionSphere->SetCollisionProfileName(TEXT("BlockAll"));
	
	BulletCollisionSphere->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel2);

	BulletCollisionSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);

	BulletCollisionSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);

	

	BulletCollisionSphere->bReturnMaterialOnMove = true;//hit event return physMaterial

	BulletCollisionSphere->SetCanEverAffectNavigation(false);//collision not affect navigation (P keybord on editor)

	RootComponent = BulletCollisionSphere;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	BulletMesh->SetupAttachment(RootComponent);
	BulletMesh->SetCanEverAffectNavigation(false);
	BulletMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
	BulletFX->SetupAttachment(RootComponent);

	BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet ProjectileMovement"));
	BulletProjectileMovement->UpdatedComponent = RootComponent;
	//BulletProjectileMovement->InitialSpeed = 2000;
	//BulletProjectileMovement->MaxSpeed = 0.f;
	
	BulletProjectileMovement->ProjectileGravityScale = 0;
	BulletProjectileMovement->bRotationFollowsVelocity = true;
	BulletProjectileMovement->bShouldBounce = false;

	InitialLifeSpan = 10;

	
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	BulletCollisionSphere->OnComponentHit.AddDynamic(this, &AProjectile::BulletCollisionSphereHit);
	
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectile::InitProjectile(FProjectileInfo InitParam, bool _ShowDebug)
{
	
	InitProjectileSpeed_Multicast(InitParam.ProjectileInitSpeed, InitParam.ProjectileMaxSpeed);
	this->SetLifeSpan(InitParam.ProjectileLifeTime);

	if (InitParam.BulletMesh)
	{
		InitProjectileMesh_Multicast(InitParam.BulletMesh, InitParam.MeshScale);
	}
	else
	{
		BulletMesh->DestroyComponent(true);
	}

	if (InitParam.BulletFX)
	{
		InitProjectileTrailFX_Multicast(InitParam.BulletFX);
	}
	else
	{
		BulletFX->DestroyComponent(true);
	}

	ShowDebug = _ShowDebug;
	ProjectileSetting = InitParam;
}

TSubclassOf<UStateEffect> AProjectile::GetStateEffectClass()
{
	return ProjectileSetting.StateEffectClass;
}

void AProjectile::ImpactProjectile()
{
	Destroy();
}

void AProjectile::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface MySurfacetype = UGameplayStatics::GetSurfaceType(Hit);

		if (ProjectileSetting.HitDecals.Contains(MySurfacetype))
		{
			UMaterialInterface* MyMaterial = ProjectileSetting.HitDecals[MySurfacetype];

			if (MyMaterial && OtherComp)
			{
				SpawnProjectileHitDecal_Multicast(MyMaterial, OtherComp, Hit);
			}
		}
		if (ProjectileSetting.HitFXs.Contains(MySurfacetype))
		{
			UParticleSystem* myParticle = ProjectileSetting.HitFXs[MySurfacetype];
			
			
				if (MySurfacetype == EPhysicalSurface::SurfaceType4)
				{
					ATopDownShooterCharacter* Character = Cast<ATopDownShooterCharacter>(OtherActor);
					if (Character)
					{
						if (Character->GetCharHealthComponent()->GetCurrentShield() > 0)
						{
							//if(ProjectileSetting.HitFXs[EPhysicalSurface::SurfaceType_Default])
							   myParticle = ProjectileSetting.HitFXs[EPhysicalSurface::SurfaceType_Default];
		
						}

					}
				}

				if (myParticle)
					SpawnProjectileHitFX_Multicast(myParticle, Hit);
			
		}

		if (ProjectileSetting.HitSounds.Contains(MySurfacetype))
		{
			USoundBase* MySound = ProjectileSetting.HitSounds[MySurfacetype];
			SpawnProjectileHitSound_Multicast(MySound, Hit);
		}

		else if(ProjectileSetting.HitSounds.Contains(EPhysicalSurface::SurfaceType_Default))
		{
			USoundBase* MySound = ProjectileSetting.HitSounds[EPhysicalSurface::SurfaceType_Default];
			SpawnProjectileHitSound_Multicast(MySound, Hit);
		}

		if (ProjectileSetting.StateEffectClass)
		{
			AProjectileGrenade* Grenade = Cast<AProjectileGrenade>(this);
			if(!Grenade)
				FuncLibrary::AddEffectBySurfaceType(OtherActor, ProjectileSetting.StateEffectClass, MySurfacetype, Hit.BoneName);
		}
		

	}
	
	
	UGameplayStatics::ApplyPointDamage(OtherActor, ProjectileSetting.ProjectileDamage, NormalImpulse, Hit,  GetInstigatorController(), this, NULL);
	UAISense_Damage::ReportDamageEvent(GetWorld(), Hit.GetActor(), GetInstigator(), ProjectileSetting.ProjectileDamage, GetInstigator()->GetActorLocation(), Hit.Location);

	ImpactProjectile();
	

}

void AProjectile::SpawnProjectileHitFX_Multicast_Implementation(UParticleSystem* HitFX, FHitResult HitResult)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitFX, FTransform(HitResult.ImpactNormal.Rotation(), HitResult.ImpactPoint, FVector(1.0f)));
}

void AProjectile::InitProjectileMesh_Multicast_Implementation(UStaticMesh* ProjectileMesh, float MeshScale)
{
	BulletMesh->SetStaticMesh(ProjectileMesh);
	FVector Scale = { MeshScale, MeshScale, MeshScale };
	BulletMesh->SetRelativeScale3D(Scale);
}



