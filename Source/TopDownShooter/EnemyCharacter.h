// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameActor.h"
#include "EnemyCharacter.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API AEnemyCharacter : public ACharacter, public IGameActor
{
	GENERATED_BODY()
	
private:

	UPROPERTY(Replicated)
	TArray<class UStateEffect*> CurrentStateEffects;

	UPROPERTY(ReplicatedUsing = EffectToAdd_OnRep)
	UStateEffect* EffectToAdd = nullptr;

	UPROPERTY(ReplicatedUsing = EffectToRemove_OnRep)
	UStateEffect* EffectToRemove = nullptr;

	UFUNCTION()
	void EffectToAdd_OnRep();

	UFUNCTION()
	void EffectToRemove_OnRep();

	void SwitchEffect(UStateEffect* _StateEffect, bool bAdd);

	virtual bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

	UFUNCTION(NetMulticast, Reliable)
	void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteEffect, FName _BoneName);

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	// Sets default values for this character's properties
	AEnemyCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	EPhysicalSurface GetSurfaceType() override;

	TArray<class UStateEffect*> GetAllCurrentEffects() override;

	void RemoveEffect(UStateEffect* RemoveEffect) override;

	void AddEffect(UStateEffect* NewEffect) override;

	FVector GetEffectPositionOffset() override;

};
