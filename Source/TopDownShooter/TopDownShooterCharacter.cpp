// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownShooterCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Weapon.h"
#include "InventoryComponent.h"
#include "TopDownShooterGameInstance.h"
#include "CharacterHealthComponent.h"
#include "Engine/DamageEvents.h"
#include "ProjectileGrenade.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine/ActorChannel.h"




void ATopDownShooterCharacter::PauseAnims()
{
	GetMesh()->bPauseAnims = true;
	
	
	if (HasAuthority() && GetController())
	{
		CharDead_BP();
		GetController()->UnPossess();
	}
}

void ATopDownShooterCharacter::EffectToAdd_OnRep()
{
	SwitchEffect(EffectToAdd, true);
	
}

void ATopDownShooterCharacter::EffectToRemove_OnRep()
{
	SwitchEffect(EffectToRemove, false);
}

void ATopDownShooterCharacter::SwitchEffect(UStateEffect* _StateEffect, bool bAdd)
{
	
	if (_StateEffect && _StateEffect->ParticleEffect)
	{
		if (bAdd) 
		{

			if (_StateEffect->ParticleEmitter == nullptr)
			{
				FName NameBoneToAttached;
				FVector Loc = FVector(0);

				NameBoneToAttached = _StateEffect->HitBoneName;
				USkeletalMeshComponent* MeshComponent = GetMesh();
				if (MeshComponent)
					_StateEffect->ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(_StateEffect->ParticleEffect, MeshComponent, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				


			}
		}
		else
		{
			_StateEffect->ParticleEmitter->DeactivateSystem();
			_StateEffect->ParticleEmitter->DestroyComponent();
			_StateEffect->ParticleEmitter = nullptr;
		}
	}
}

void ATopDownShooterCharacter::CheckAmmoForWeapon()
{
	int Ammo = InventoryComponent->CheckAmmoForWeapon(CurrentWeapon->WeaponSetting.WeaponType);
	if (Ammo <= 0)
	{
		OnNoAmmo_BP();
	}
	else
	{
		OnHaveAmmo_BP();
	}
}

void ATopDownShooterCharacter::SetMovementState_OnServer_Implementation(EMovementState _MovementState)
{
	SetMovementState_Multicast(_MovementState);
	
}

void ATopDownShooterCharacter::SetMovementState_Multicast_Implementation(EMovementState _MovementState)
{
	MovementState = _MovementState;
	CharacterUpdate();
}

void ATopDownShooterCharacter::SetActorRotationByYaw_Multicast_Implementation(float Yaw)
{
	
	if (GetController() && GetController()->IsLocalPlayerController())
	{
		SetActorRotation(FRotator(0, Yaw, 0));
	}
}

void ATopDownShooterCharacter::SetActorRotationByYaw_OnServer_Implementation(float Yaw)
{
	//SetActorRotationByYaw_Multicast(Yaw);
	SetActorRotation(FRotator(0, Yaw, 0));
}



void ATopDownShooterCharacter::CharDead_BP_Implementation()
{
	//BP
}

ATopDownShooterCharacter::ATopDownShooterCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	SetCanBeDamaged(true);

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	CharHealthComponent = CreateDefaultSubobject<UCharacterHealthComponent>(TEXT("CharHealth"));
	CharHealthComponent->OnDead.AddDynamic(this, &ATopDownShooterCharacter::CharDead);

	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
	InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATopDownShooterCharacter::InitWeapon);
	InventoryComponent->OnUpdateWeaponSlot.AddDynamic(this, &ATopDownShooterCharacter::OnUpdateWeaponSlot);
	InventoryComponent->OnUpdateWeaponCurrentRounds.AddDynamic(this, &ATopDownShooterCharacter::OnUpdateWeaponCurrentRounds_BP);
	InventoryComponent->OnAmmoChange.AddDynamic(this, &ATopDownShooterCharacter::OnAmmoChange_BP);

	

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATopDownShooterCharacter::BeginPlay()
{
	Super::BeginPlay();
	CurrentStamina = Stamina;

	
	/*if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}*/

	
}

void ATopDownShooterCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
	MovementTick(DeltaSeconds);
	//CursorTick();
	
}


void ATopDownShooterCharacter::SetupPlayerInputComponent(UInputComponent* _InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
	_InputComponent->BindAxis(TEXT("Move Forward"), this, &ATopDownShooterCharacter::InputAxisX);
	_InputComponent->BindAxis(TEXT("Move Right"), this, &ATopDownShooterCharacter::InputAxisY);

	_InputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::InputFirePressed);
	_InputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATopDownShooterCharacter::InputFireReleased);
	_InputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::InputReloadPressed);
	_InputComponent->BindAction(TEXT("NextWeapon"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::InputNextWeapon);
	_InputComponent->BindAction(TEXT("PreviousWeapon"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::InputPreviousWeapon);
}

void ATopDownShooterCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATopDownShooterCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATopDownShooterCharacter::MovementTick(float DeltaSeconds)
{
	if (!CharHealthComponent->GetIsAlive() || !bIsPlayerPawn) return;
	if (!GetController() || !GetController()->IsLocalPlayerController()) return;
	
	AddMovementInput(FVector(1, 0, 0), AxisX);
	AddMovementInput(FVector(0, 1, 0), AxisY);
	APlayerController* PlayerController = Cast<APlayerController>(GetController());
	if (PlayerController)
	{
		FHitResult HitResult;
		PlayerController->GetHitResultUnderCursor(ECollisionChannel::ECC_GameTraceChannel1, true, HitResult);
		auto Rotator = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location);
		SetActorRotation(FRotator(0, Rotator.Yaw, 0));

		SetActorRotationByYaw_OnServer(Rotator.Yaw);

		if (CurrentWeapon)
		{
			CurrentWeapon->UpdateShootEndLocation_OnServer(HitResult.Location);
		}
		
	}


	if (MovementState == EMovementState::SPRINT_STATE)
	{
		CharacterSprint(DeltaSeconds);
	}
	else if (CurrentStamina < Stamina)
	{
		CurrentStamina += DeltaSeconds * 30;
	}

	if (CurrentStamina > Stamina)
	{
		CurrentStamina = Stamina;
	}

}

void ATopDownShooterCharacter::CursorTick()
{
	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}
}

void ATopDownShooterCharacter::CharacterUpdate()
{
	float ResSpeed = MovementSpeed.RunSpeed;
	switch (MovementState)
	{
	case EMovementState::AIM_STATE:
		AimEnabled = true;
		ResSpeed = MovementSpeed.AimSpeed;
		break;
	case EMovementState::AIM_WALK_STATE:
		AimEnabled = true;
		ResSpeed = MovementSpeed.AimWalkSpeed;
		break;
	case EMovementState::WALK_STATE:
		AimEnabled = false;
		ResSpeed = MovementSpeed.WalkSpeed;
		break;
	case EMovementState::RUN_STATE:
		AimEnabled = false;
		ResSpeed = MovementSpeed.RunSpeed;
		break;
	case EMovementState::SPRINT_STATE:
		AimEnabled = false;
		break;
	default:
		break;
	}

	if (MovementState == EMovementState::SPRINT_STATE)
	{
		if (GetCharacterMovement()->MaxWalkSpeed < MovementSpeed.RunSpeed)
		{
			GetCharacterMovement()->MaxWalkSpeed = MovementSpeed.RunSpeed;
	    }
	}
	else
	{
		GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
	}
	
	
}

void ATopDownShooterCharacter::CharacterSprint(float DeltaSeconds)
{
	auto Movement = GetCharacterMovement();
	if (Movement->MaxWalkSpeed < 800)
	{
		Movement->MaxWalkSpeed += DeltaSeconds * 150;
	}
	else
	{
		Movement->MaxWalkSpeed = 800;
		
	}

	CurrentStamina -= DeltaSeconds * 30;
}


bool ATopDownShooterCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{

	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (UStateEffect* StateEffect : CurrentStateEffects)
	{
		WroteSomething |= Channel->ReplicateSubobject(StateEffect, *Bunch, *RepFlags);
	}
	return WroteSomething;
}

void ATopDownShooterCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteEffect, FName _BoneName)
{
	if (ExecuteEffect)
	{
		
		USkeletalMeshComponent* MeshComponent = GetMesh();
		if (MeshComponent)
			UGameplayStatics::SpawnEmitterAttached(ExecuteEffect, MeshComponent, _BoneName, FVector(0), FRotator::ZeroRotator, EAttachLocation::SnapToTarget, true);

	}
		
}

void ATopDownShooterCharacter::InitWeapon(FName IdWeapon, int CurrentRounds, int WeaponSlotIndex)
{
	//OnServer

	UTopDownShooterGameInstance* MyGI = Cast<UTopDownShooterGameInstance>(GetGameInstance());
	FWeaponInfo MyWeaponInfo;
	if (MyGI)
	{
		if (MyGI->GetWeaponInfoByName(IdWeapon, MyWeaponInfo))
		{
			if (MyWeaponInfo.WeaponClass)
			{
				if (CurrentWeapon)
				{
					CurrentWeapon->Destroy();
					CurrentWeapon = nullptr;
				}

				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = this;

				AWeapon* MyWeapon = Cast<AWeapon>(GetWorld()->SpawnActor(MyWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (MyWeapon)
				{
					

					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					MyWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = MyWeapon;
					if (CurrentRounds > MyWeaponInfo.MaxRound)
						CurrentRounds = MyWeaponInfo.MaxRound;

					CurrentWeapon->WeaponInit(MyWeaponInfo, CurrentRounds);
					CurrentWeapon->UpdateStateWeapon_OnServer(MovementState);
					
					MyWeapon->OnWeaponReloadStart.AddDynamic(this, &ATopDownShooterCharacter::WeaponReloadStart);
					MyWeapon->OnWeaponFire.AddDynamic(this, &ATopDownShooterCharacter::OnWeaponFire);
					MyWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATopDownShooterCharacter::WeaponReloadEnd);
					MyWeapon->OnTryReloadWeapon.AddDynamic(this, &ATopDownShooterCharacter::TryReloadWeapon_OnServer);
					MyWeapon->OnNoWeaponCurrentRounds.AddDynamic(this, &ATopDownShooterCharacter::CheckAmmoForWeapon);
					

					int PreviousWeaponIndex = CurrentWeaponSlotIndex;
					CurrentWeaponSlotIndex = WeaponSlotIndex;
					
					OnSwitchWeapon_BP(CurrentWeaponSlotIndex, PreviousWeaponIndex, CurrentWeapon->WeaponSetting.WeaponType);

					if (CurrentRounds <= 0)
					{
						CheckAmmoForWeapon();
					}
					else
					{
						OnHaveAmmo_BP();
					}
					
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATopDownShooterCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}

void ATopDownShooterCharacter::InputFirePressed()
{
	if (!CharHealthComponent->GetIsAlive()) return;

	AttackCharEvent(true);
	
}

void ATopDownShooterCharacter::InputFireReleased()
{
	AttackCharEvent(false);
}

void ATopDownShooterCharacter::InputNextWeapon()
{
	
	InventoryComponent->SwitchWeapon_OnServer(CurrentWeaponSlotIndex + 1, true, true);
}

void ATopDownShooterCharacter::InputPreviousWeapon()
{
	InventoryComponent->SwitchWeapon_OnServer(CurrentWeaponSlotIndex - 1, false, true);
}

void ATopDownShooterCharacter::AttackCharEvent(bool bIsFiring)
{	
	if (CurrentWeapon)
	{
		
		CurrentWeapon->SetWeaponStateFire_OnServer(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

void ATopDownShooterCharacter::DropItem_BP_Implementation(FDropItem DropItemInfo, EWeaponType WeaponType)
{
	//BP
}

void ATopDownShooterCharacter::WeaponReloadStart(float ReloadTime, UAnimMontage* Anim, float PlayRate)
{
	WeaponReloadStart_BP(CurrentWeaponSlotIndex, ReloadTime, Anim, PlayRate);
}

void ATopDownShooterCharacter::WeaponReloadEnd(int RoundsTaken)
{
	InventoryComponent->ChangeWeaponCurrentRounds(CurrentWeaponSlotIndex, CurrentWeapon->GetCurrentRounds());
	InventoryComponent->ChangeAmmoCount(CurrentWeapon->WeaponSetting.WeaponType, -RoundsTaken);
}

void ATopDownShooterCharacter::OnWeaponFire(UAnimMontage* AnimFireHip, UAnimMontage* AnimFireAim)
{
	InventoryComponent->ChangeWeaponCurrentRounds(CurrentWeaponSlotIndex, CurrentWeapon->GetCurrentRounds());

	if (MovementState == EMovementState::AIM_STATE || MovementState == EMovementState::AIM_WALK_STATE)
	{
		OnWeaponFire_BP(AnimFireAim);
	}
	else
	{
		OnWeaponFire_BP(AnimFireHip);
	}
	
}

void ATopDownShooterCharacter::InputReloadPressed()
{
	if (CurrentWeapon)
	{
		TryReloadWeapon_OnServer();
		
	}
	
}

void ATopDownShooterCharacter::OnUpdateWeaponSlot(int WeaponIndex, FWeaponSlot WeaponSlot)
{
	UTopDownShooterGameInstance* MyGI = Cast<UTopDownShooterGameInstance>(GetGameInstance());
	FWeaponInfo MyWeaponInfo;
	if (MyGI)
	{
		if (MyGI->GetWeaponInfoByName(WeaponSlot.NameItem, MyWeaponInfo))
		{
			/*if (WeaponSlot.CurrentRounds > MyWeaponInfo.MaxRound)
			{
				WeaponSlot.CurrentRounds = MyWeaponInfo.MaxRound;
				InventoryComponent->ChangeWeaponCurrentRounds(WeaponIndex, WeaponSlot.CurrentRounds);
			}*/

			OnUpdateWeaponSlot_BP(WeaponIndex, MyWeaponInfo.WeaponIcon, WeaponSlot.CurrentRounds, MyWeaponInfo.MaxRound, MyWeaponInfo.WeaponType);
		}
	}
}

void ATopDownShooterCharacter::TryReloadWeapon_OnServer_Implementation()
{
	
	int Ammo = InventoryComponent->CheckAmmoForWeapon(CurrentWeapon->WeaponSetting.WeaponType);
	if (Ammo > 0)
	{

		CurrentWeapon->InitReload(Ammo);
	}
}

void ATopDownShooterCharacter::OnSwitchWeapon_BP_Implementation(int WeaponIndex, int PreviousWeaponIndex, EWeaponType WeaponType)
{

}

void ATopDownShooterCharacter::OnAmmoChange_BP_Implementation(int AmmoSlotIndex, int AmmoCount)
{

}

void ATopDownShooterCharacter::OnUpdateWeaponCurrentRounds_BP_Implementation(int WeaponIndex, int CurrentRounds)
{

}



void ATopDownShooterCharacter::OnUpdateWeaponSlot_BP_Implementation(int WeaponIndex, UTexture2D* WeaponIcon, int CurrentRounds, int MaxRounds, EWeaponType WeaponType)
{

}

void ATopDownShooterCharacter::OnWeaponFire_BP_Implementation(UAnimMontage* Anim)
{

}

void ATopDownShooterCharacter::WeaponReloadStart_BP_Implementation(int WeaponIndex, float ReloadTime, UAnimMontage* Anim, float PlayRate)
{
    
}



void ATopDownShooterCharacter::ChangeMovementState()
{
	

	bool IsMoving = GetCharacterMovement()->Velocity != FVector::ZeroVector;
	bool IsMovingForward = Direction <= 20 && Direction >= -20;
	

	if (SprintEnabled && IsMoving && IsMovingForward && CurrentStamina > 0)
	{
		MovementState = EMovementState::SPRINT_STATE;
		
	}else if (WalkEnabled && AimEnabled && !SprintEnabled)
	{
		MovementState = EMovementState::AIM_WALK_STATE;
	
	}else if (WalkEnabled && !SprintEnabled)
	{
		MovementState = EMovementState::WALK_STATE;
		
	}else if (AimEnabled && !SprintEnabled)
	{
		MovementState = EMovementState::AIM_STATE;
	}
	else
	{
		MovementState = EMovementState::RUN_STATE;
	}

	
	SetMovementState_OnServer(MovementState);

	

	if (CurrentWeapon)
	{
		CurrentWeapon->UpdateStateWeapon_OnServer(MovementState);
	}
}

bool ATopDownShooterCharacter::TryPickupAmmo(AActor* PickupActor, EWeaponType WeaponType, int AmmoCount)
{
	
	bool Result = InventoryComponent->ChangeAmmoCount(WeaponType, AmmoCount);

	if (Result)
	{
		OnHaveAmmo_BP();
	}

	return Result;
}



void ATopDownShooterCharacter::TrySwitchWeaponToInventory_OnServer_Implementation(AActor* PickupActor, int WeaponIndex, FDropItem PickupWeaponItem)
{
	UTopDownShooterGameInstance* MyGI = Cast<UTopDownShooterGameInstance>(GetGameInstance());
	FWeaponInfo MyWeaponInfo;
	if (MyGI)
	{
		FWeaponSlot WeaponSlot;
		if (InventoryComponent->GetWeaponSlotByIndex(WeaponIndex, WeaponSlot) && MyGI->GetWeaponInfoByName(WeaponSlot.NameItem, MyWeaponInfo))
		{

			InventoryComponent->UpdateWeaponSlot(WeaponIndex, PickupWeaponItem.WeaponSlotInfo);
			if (CurrentWeaponSlotIndex == WeaponIndex)
			{
				InventoryComponent->SwitchWeapon_OnServer(WeaponIndex, true, false);
			}

			
			/*DropWeaponItem = MyWeaponInfo.DropWeaponInfo;
			DropWeaponItem.WeaponSlotInfo.CurrentRounds = WeaponSlot.CurrentRounds;
			DropWeaponType = MyWeaponInfo.WeaponType;*/
			MyWeaponInfo.DropWeaponInfo.WeaponSlotInfo.CurrentRounds = WeaponSlot.CurrentRounds;
			PickupActor->Destroy();
			DropItem_BP(MyWeaponInfo.DropWeaponInfo, MyWeaponInfo.WeaponType);
			
		}
	}

	
}



void ATopDownShooterCharacter::SwitchWeapon(int WeaponIndex)
{
	if (WeaponIndex == CurrentWeaponSlotIndex)
	{
		return;
	}
	InventoryComponent->SwitchWeapon_OnServer(WeaponIndex, true, false);
}

EPhysicalSurface ATopDownShooterCharacter::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	
	if (GetMesh())
	{
		Result = GetMesh()->GetBodyInstance()->GetSimplePhysicalMaterial()->SurfaceType;
	}

	return Result;
}

TArray<class UStateEffect*> ATopDownShooterCharacter::GetAllCurrentEffects()
{
	return CurrentStateEffects;
}

void ATopDownShooterCharacter::RemoveEffect(UStateEffect* RemoveEffect)
{
	CurrentStateEffects.Remove(RemoveEffect);
	if (!RemoveEffect->bIsAutoDestroy)
	{
		EffectToRemove = RemoveEffect;
		SwitchEffect(EffectToRemove, false);
	}
}

void ATopDownShooterCharacter::AddEffect(UStateEffect* NewEffect)
{
    
	CurrentStateEffects.Add(NewEffect);
	if (NewEffect->bIsAutoDestroy)
	{
		if (NewEffect->ParticleEffect)
			ExecuteEffectAdded_Multicast(NewEffect->ParticleEffect, NewEffect->HitBoneName);
	}
	else
	{
		EffectToAdd = NewEffect;
		SwitchEffect(EffectToAdd, true);
	}
}



FVector ATopDownShooterCharacter::GetEffectPositionOffset()
{
	return FVector();
}

//bool ATopDownShooterCharacter::CharIsDead()
//{
//	//return bIsDead;
//}



void ATopDownShooterCharacter::OnNoAmmo_BP_Implementation()
{
}

void ATopDownShooterCharacter::OnHaveAmmo_BP_Implementation()
{
}



float ATopDownShooterCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (CharHealthComponent->GetIsAlive())
	{
		for (int i = 0; i < GetAllCurrentEffects().Num(); i++)
		{
			if (GetAllCurrentEffects()[i]->EffectName == FName("Invulnarability effect"))
			{

				return ActualDamage;
				
			}

		}

		AProjectileGrenade* Grenade = Cast<AProjectileGrenade>(DamageCauser);
		if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID) && Grenade)
		{
			FuncLibrary::AddEffectBySurfaceType(this, Grenade->GetStateEffectClass(), GetSurfaceType(), FName(TEXT("spine_02")));
			
		}
		
		CharHealthComponent->ChangeHealthValue_OnServer(-ActualDamage);
	}
	
	return ActualDamage;
}

void ATopDownShooterCharacter::CharDead()
{
	

	int rnd = FMath::RandRange(0, DeadsAnim.Num() - 1);

	if (DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		if (DeadsAnim.IsValidIndex(rnd))
		{

			float TimeAnim = GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
			GetWorldTimerManager().SetTimer(PauseAnimsTimer, this, &ATopDownShooterCharacter::PauseAnims, TimeAnim, false);
		}

	}

	//bIsDead = true;

	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);

	AttackCharEvent(false);

}

void ATopDownShooterCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATopDownShooterCharacter, CurrentWeapon);
	DOREPLIFETIME(ATopDownShooterCharacter, CurrentWeaponSlotIndex);
	DOREPLIFETIME(ATopDownShooterCharacter, CurrentStateEffects);
	DOREPLIFETIME(ATopDownShooterCharacter, EffectToAdd);
	DOREPLIFETIME(ATopDownShooterCharacter, EffectToRemove);
}

