// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProjectTypes.h"
#include "Projectile.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API AProjectile : public AActor
{
	GENERATED_BODY()

private:

	UFUNCTION()
	void BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION(NetMulticast, Reliable)
	void InitProjectileMesh_Multicast(UStaticMesh* ProjectileMesh, float MeshScale);

	UFUNCTION(NetMulticast, Reliable)
	void InitProjectileTrailFX_Multicast(UParticleSystem* TrailFX);

	UFUNCTION(NetMulticast, Reliable)
	void SpawnProjectileHitFX_Multicast(UParticleSystem* HitFX, FHitResult HitResult);

	UFUNCTION(NetMulticast, Reliable)
	void SpawnProjectileHitSound_Multicast(USoundBase* HitSound, FHitResult HitResult);

	UFUNCTION(NetMulticast, Reliable)
	void SpawnProjectileHitDecal_Multicast(UMaterialInterface* Decal, UPrimitiveComponent* OtherComp, FHitResult HitResult);

	UFUNCTION(NetMulticast, Reliable)
	void InitProjectileSpeed_Multicast(float InitSpeed, float MaxSpeed);

	void PostNetReceiveVelocity(const FVector& NewVelocity) override;
	
public:	
	// Sets default values for this actor's properties
	AProjectile();

	
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void ImpactProjectile();

	FProjectileInfo ProjectileSetting;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UStaticMeshComponent* BulletMesh = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USphereComponent* BulletCollisionSphere = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UProjectileMovementComponent* BulletProjectileMovement = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UParticleSystemComponent* BulletFX = nullptr;

	bool ShowDebug = false;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void InitProjectile(FProjectileInfo InitParam, bool _ShowDebug);

	TSubclassOf<UStateEffect> GetStateEffectClass();
};
