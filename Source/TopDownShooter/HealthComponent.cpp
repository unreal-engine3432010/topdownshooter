// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "Net/UnrealNetwork.h"



void UHealthComponent::HealthChangeEvent_Multicast_Implementation(float _Health, float _ChangeValue)
{
	OnHealthChange.Broadcast(_Health, _ChangeValue);
}

void UHealthComponent::DeadEvent_Multicast_Implementation()
{
	OnDead.Broadcast();
}

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicatedByDefault(true);
	
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UHealthComponent::ChangeHealthValue_OnServer_Implementation(float ChangeValue)
{
	if (!bIsAlive) return;

	float NewValue = ChangeValue;

	if (ChangeValue < 0)
	{
		NewValue *= CoefDamage;
	}

	Health += NewValue;

	if (Health > 100)
	{
		Health = 100;
	}

	if (Health < 0)
	{
		Health = 0;
	}

	HealthChangeEvent_Multicast(Health, ChangeValue);
	

	if (Health <= 0)
	{
		bIsAlive = false;
		DeadEvent_Multicast();
	}
}

float UHealthComponent::GetCurrentHealth()
{
	return Health;
}

bool UHealthComponent::GetIsAlive()
{
	return bIsAlive;
}


// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UHealthComponent, Health);
	DOREPLIFETIME(UHealthComponent, bIsAlive);
	
}

