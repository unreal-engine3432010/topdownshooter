// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"
#include "Components/ArrowComponent.h"
#include "Projectile.h"
#include "ProjectileGrenade.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/StaticMeshActor.h"
#include "Net/UnrealNetwork.h"
#include "Perception/AISense_Damage.h"

// Sets default values
AWeapon::AWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);

	DropMagazineLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("DropMagazineLocation"));
	DropMagazineLocation->SetupAttachment(RootComponent);

	DropShellLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("DropShellLocation"));
	DropShellLocation->SetupAttachment(RootComponent);

	bReplicates = true;
}


void InitBulletShell(AActor* _Actor)
{


	_Actor->InitialLifeSpan = 3;

}

void AWeapon::PlayWeaponAnim_Multicast_Implementation(UAnimMontage* Anim, float PlayRate)
{
	SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(Anim, PlayRate);
}

void AWeapon::SpawnWeaponFireFX_Multicast_Implementation(UParticleSystem* FireFX, USoundBase* FireSound)
{
	if(FireSound)
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), FireSound, ShootLocation->GetComponentLocation());

	if(FireFX)
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FireFX, ShootLocation->GetComponentTransform());
}

void AWeapon::SpawnHitDecal_Multicast_Implementation(UMaterialInterface* Decal, FHitResult HitResult)
{
	UGameplayStatics::SpawnDecalAttached(Decal, FVector(20.0f), HitResult.GetComponent(), NAME_None, HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
}

void AWeapon::SpawnHitFX_Multicast_Implementation(UParticleSystem* HitFX, FHitResult HitResult)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitFX, FTransform(HitResult.ImpactNormal.Rotation(), HitResult.ImpactPoint, FVector(1.0f)));
}

void AWeapon::SpawnHitSound_Multicast_Implementation(USoundBase* HitSound, FHitResult HitResult)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, HitResult.ImpactPoint);
}

// Called when the game starts or when spawned
void AWeapon::BeginPlay()
{
	
	Super::BeginPlay();
	
}

// Called every frame
void AWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DropMagazineTick(DeltaTime);
	DispersionTick(DeltaTime);
}

void AWeapon::WeaponInit(FWeaponInfo WeaponInfo, int _CurrentRounds)
{
	if (!SkeletalMeshWeapon->GetSkeletalMeshAsset())
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (!StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}

	WeaponSetting = WeaponInfo;
	CurrentRounds = _CurrentRounds;
	if(CurrentRounds <= 0)
	{
		OnNoWeaponCurrentRounds.Broadcast();
	}

	DropMagazineTimer = WeaponSetting.DropMeshInfo.DropMagazineTime;
}

void AWeapon::SetWeaponStateFire_OnServer_Implementation(bool bIsFire)
{
	
	bIsWeaponFiring = bIsFire;
	
}

int8 AWeapon::GetCurrentRounds()
{
	return CurrentRounds;
}

void AWeapon::Fire()
{
	//On Server

	if (CurrentRounds <= 0)
	{
		OnTryReloadWeapon.Broadcast();
		return;
	}

	FireTimer = WeaponSetting.RateOfFire;
	CurrentDispersion += CurrentDispersionRecoil;

	CurrentRounds --;
	WeaponSetting.DropWeaponInfo.WeaponSlotInfo.CurrentRounds = CurrentRounds;

	if (CurrentRounds <= 0)
	{
		OnNoWeaponCurrentRounds.Broadcast();
	}
		
		OnWeaponFire.Broadcast(WeaponSetting.WeaponAnimation.AnimCharFireHip, WeaponSetting.WeaponAnimation.AnimCharFireAim);
	

	int8 NumberProjectile = WeaponSetting.NumberProjectileByShot;

	if (ShootLocation)
	{
		if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
		{
			if (WeaponSetting.WeaponAnimation.WeaponFire)
			{
				PlayWeaponAnim_Multicast(WeaponSetting.WeaponAnimation.WeaponFire, 1);
			}
		}

		if (!WeaponSetting.WeaponAnimation.WeaponFire)
		{
			if (WeaponSetting.SoundFireWeapon || WeaponSetting.EffectFireWeapon)
				SpawnWeaponFireFX_Multicast(WeaponSetting.EffectFireWeapon, WeaponSetting.SoundFireWeapon);
		}

		
		DropBulletShell_Multicast(WeaponSetting.DropMeshInfo);
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation;
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = WeaponSetting.ProjectileSetting;

		FVector EndLocation;



		for (int8 i = 0; i < NumberProjectile; i++)//Shotgun
		{
			EndLocation = GetFireEndLocation();

			SpawnRotation = UKismetMathLibrary::FindLookAtRotation(SpawnLocation, EndLocation);

			if (WeaponSetting.IsGrenadeLauncher)
			{
				SpawnRotation += FRotator(WeaponSetting.GrenadeShootAngle, 0, 0);
			}

		

			if (ProjectileInfo.BulletFX || ProjectileInfo.BulletMesh)
			{
				//Projectile Init ballistic fire

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectile* MyProjectile = nullptr;

				if (WeaponSetting.IsGrenadeLauncher)
				{
				     MyProjectile = Cast<AProjectile>(GetWorld()->SpawnActor(AProjectileGrenade::StaticClass(), &SpawnLocation, &SpawnRotation, SpawnParams));
				}
				else
				{
					MyProjectile = Cast<AProjectile>(GetWorld()->SpawnActor(AProjectile::StaticClass(), &SpawnLocation, &SpawnRotation, SpawnParams));
				}
				if (MyProjectile)
				{
					MyProjectile->InitProjectile(WeaponSetting.ProjectileSetting, ShowDebug);
				}
			}
			else
			{
				FHitResult HitResult;
				FCollisionQueryParams QueryParams;
				QueryParams.bReturnPhysicalMaterial = true;

				if (GetWorld()->LineTraceSingleByChannel(HitResult, ShootLocation->GetComponentLocation(), EndLocation, ECollisionChannel::ECC_GameTraceChannel3, QueryParams))
				{
					
					if (HitResult.GetActor() && HitResult.PhysMaterial.IsValid())
					{
						EPhysicalSurface MySurfacetype = UGameplayStatics::GetSurfaceType(HitResult);

						if (WeaponSetting.ProjectileSetting.HitDecals.Contains(MySurfacetype))
						{
							UMaterialInterface* MyMaterial = WeaponSetting.ProjectileSetting.HitDecals[MySurfacetype];

							if (MyMaterial && HitResult.GetComponent())
							{
								SpawnHitDecal_Multicast(MyMaterial, HitResult);
							}
						}
						if (WeaponSetting.ProjectileSetting.HitFXs.Contains(MySurfacetype))
						{
							UParticleSystem* myParticle = WeaponSetting.ProjectileSetting.HitFXs[MySurfacetype];
							if (myParticle)
							{
								SpawnHitFX_Multicast(myParticle, HitResult);
							}
						}

						if (WeaponSetting.ProjectileSetting.HitSounds.Contains(MySurfacetype))
						{
							USoundBase* MySound = WeaponSetting.ProjectileSetting.HitSounds[MySurfacetype];
							SpawnHitSound_Multicast(MySound, HitResult);
						}

						if (WeaponSetting.ProjectileSetting.StateEffectClass)
						{
							FuncLibrary::AddEffectBySurfaceType(HitResult.GetActor(), WeaponSetting.ProjectileSetting.StateEffectClass, MySurfacetype, HitResult.BoneName);
						}

						UGameplayStatics::ApplyPointDamage(HitResult.GetActor(), WeaponSetting.WeaponDamage, HitResult.ImpactNormal, HitResult, GetInstigatorController(), this, NULL);
						UAISense_Damage::ReportDamageEvent(GetWorld(), HitResult.GetActor(), GetInstigator(), WeaponSetting.WeaponDamage, GetInstigator()->GetActorLocation(), HitResult.Location);

					}
				}

			}
		}
	}
}



void AWeapon::FireTick(float DeltaTime)
{
	
	
	if (bIsWeaponFiring && !bIsWeaponReloading)
	{
		
		if (FireTimer <= 0)
		{
			
				Fire();
		}
	}

		if (FireTimer > 0)
		{
			FireTimer -= DeltaTime;
		}
			
	
	
}


void AWeapon::ReloadTick(float DeltaTime)
{
	if (bIsWeaponReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeapon::InitReload(int Ammo)
{
	//On server

	if (CurrentRounds == WeaponSetting.MaxRound || bIsWeaponReloading)
	{
		return;
	}

	int NeedRoundsForReloading = WeaponSetting.MaxRound - CurrentRounds;
	if (Ammo >= WeaponSetting.MaxRound)
	{
		RoundsForReloading = NeedRoundsForReloading;
	}
	else if(Ammo >= NeedRoundsForReloading)
	{
		RoundsForReloading = NeedRoundsForReloading;
	}
	else
	{
		RoundsForReloading = Ammo;
	}

	bIsWeaponReloading = true;

	ReloadTimer = WeaponSetting.ReloadTime;

	

	
	if (SkeletalMeshWeapon)
	{
		if (WeaponSetting.WeaponAnimation.WeaponReload)
		{
			if (SkeletalMeshWeapon->GetAnimInstance())
			{
				float PlayRate = WeaponSetting.WeaponAnimation.WeaponReload->GetPlayLength() / WeaponSetting.ReloadTime;
				PlayWeaponAnim_Multicast(WeaponSetting.WeaponAnimation.WeaponReload, PlayRate);
		
			}
			
		}
			
	}

	if (!WeaponSetting.WeaponAnimation.WeaponReload)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundReloadWeapon, ShootLocation->GetComponentLocation());
	}
	
	DropMagazineFlag = true;

	//ToDo Anim reload
	if (WeaponSetting.WeaponAnimation.AnimCharReload)
	{
		float PlayRate =  WeaponSetting.WeaponAnimation.AnimCharReload->GetPlayLength() / WeaponSetting.ReloadTime;
		OnWeaponReloadStart.Broadcast(WeaponSetting.ReloadTime, WeaponSetting.WeaponAnimation.AnimCharReload, PlayRate);
	}
}

void AWeapon::UpdateShootEndLocation_OnServer_Implementation(FVector NewShootEndLocation)
{
	ShootEndLocation = NewShootEndLocation;
}

void AWeapon::FinishReload()
{
	bIsWeaponReloading = false;
	CurrentRounds += RoundsForReloading;
	
	OnWeaponReloadEnd.Broadcast(RoundsForReloading);
}

FVector AWeapon::GetFireEndLocation()
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.f);

	ShootEndLocation = FVector(ShootEndLocation.X, ShootEndLocation.Y, ShootLocation->GetComponentLocation().Z);
	
	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);
	//UE_LOG(LogTemp, Warning, TEXT("Vector: X = %f. Y = %f. Size = %f"), tmpV.X, tmpV.Y, tmpV.Size());

	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSetting.DistanceTrace, CurrentDispersion * PI / 180.f, CurrentDispersion * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSetting.DistanceTrace, CurrentDispersion * PI / 180.f, CurrentDispersion * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}


	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);

	}


	return EndLocation;
}

FVector AWeapon::ApplyDispersionToShoot(FVector DirectionShoot)
{
	return FMath::VRandCone(DirectionShoot, CurrentDispersion * PI / 180.f);
}

void AWeapon::DropMagazine_Multicast_Implementation(FDropMeshInfo DropMeshInfo)
{
	if (!DropMeshInfo.MagazineDrop)
		return;

	FVector SpawnLocation = DropMagazineLocation->GetComponentLocation();
	FRotator SpawnRotation = DropMagazineLocation->GetComponentRotation();
	FTransform SpawnTransform;
	SpawnTransform.SetLocation(SpawnLocation);
	SpawnTransform.SetRotation(FQuat(SpawnRotation));

	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.Owner = this;
	SpawnParams.Instigator = GetInstigator();
	//SpawnParams.CustomPreSpawnInitalization

	AMagazine* MagazineActor = nullptr;
	MagazineActor = GetWorld()->SpawnActor<AMagazine>(DropMeshInfo.MagazineDrop, SpawnTransform, SpawnParams);
	MagazineActor->SetMagazineMass(DropMeshInfo.MagazineMass);
}

void AWeapon::DropMagazineTick(float DeltaTime)
{
	if (DropMagazineFlag)
	{
		if (DropMagazineTimer <= 0)
		{
			DropMagazine_Multicast(WeaponSetting.DropMeshInfo);
			DropMagazineTimer = WeaponSetting.DropMeshInfo.DropMagazineTime;
			DropMagazineFlag = false;
		}
		else
		{
			DropMagazineTimer -= DeltaTime;
		}
	}
}

void AWeapon::DropBulletShell_Multicast_Implementation(FDropMeshInfo DropMeshInfo)
{
	if (!DropMeshInfo.BulletShellMesh)
	{
		return;
	}

	FVector SpawnLocation = DropShellLocation->GetComponentLocation();
	FRotator SpawnRotation = DropShellLocation->GetComponentRotation();
	FTransform SpawnTransform;
	SpawnTransform.SetLocation(SpawnLocation);
	SpawnTransform.SetRotation(FQuat(SpawnRotation));
	SpawnTransform.SetScale3D(FVector(1.5, 1.5, 1.5));

	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.Owner = GetOwner();
	SpawnParams.Instigator = GetInstigator();
	SpawnParams.CustomPreSpawnInitalization = InitBulletShell;

	AStaticMeshActor* ShellActor = nullptr;
	ShellActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), SpawnTransform, SpawnParams);
	ShellActor->SetActorTickEnabled(false);
	ShellActor->SetMobility(EComponentMobility::Movable);
	ShellActor->GetStaticMeshComponent()->SetComponentTickEnabled(false);
	ShellActor->GetStaticMeshComponent()->SetGenerateOverlapEvents(false);
	
	ShellActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
	ShellActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	ShellActor->GetStaticMeshComponent()->SetMassOverrideInKg(TEXT(""), 0.1, true);
	
	ShellActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
	ShellActor->GetStaticMeshComponent()->SetStaticMesh(DropMeshInfo.BulletShellMesh);
	
	FVector ImpulseDir = FMath::VRandCone(DropShellLocation->GetRightVector(), DropMeshInfo.DropShellImpulseDispersion * PI / 180.f);
	
	ShellActor->GetStaticMeshComponent()->AddImpulse(ImpulseDir * DropMeshInfo.DropShellImpulsePower);
}



void AWeapon::DispersionTick(float DeltaTime)
{
	
		if (!bIsWeaponFiring)
		{
			
				CurrentDispersion -= CurrentDispersionReduction;
			
		}
		

		if (CurrentDispersion < CurrentDispersionMin)
		{

			CurrentDispersion = CurrentDispersionMin;

		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	
	if (ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
}

void AWeapon::UpdateStateWeapon_OnServer_Implementation(EMovementState NewMovementState)
{
	//ToDo Dispersion
	//BlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::AIM_STATE:

		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::AIM_WALK_STATE:

		CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::WALK_STATE:

		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::RUN_STATE:

		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::SPRINT_STATE:
		//BlockFire = true;
		SetWeaponStateFire_OnServer(false);//set fire trigger to false
		//Block Fire
		break;
	default:
		break;
	}

}


void AWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWeapon, CurrentRounds);
}


