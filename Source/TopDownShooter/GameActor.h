// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "GameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */

class TOPDOWNSHOOTER_API IGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	
	 virtual EPhysicalSurface GetSurfaceType() = 0;

	
	 virtual TArray<class UStateEffect*> GetAllCurrentEffects() = 0;

	
	virtual  void RemoveEffect(UStateEffect* RemoveEffect) = 0;

	
	virtual void AddEffect(UStateEffect* NewEffect) = 0;

	

	 virtual FVector GetEffectPositionOffset() = 0;

	 
};
