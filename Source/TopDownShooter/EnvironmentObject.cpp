// Fill out your copyright notice in the Description page of Project Settings.


#include "EnvironmentObject.h"

// Sets default values
AEnvironmentObject::AEnvironmentObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}



// Called when the game starts or when spawned
void AEnvironmentObject::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEnvironmentObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface AEnvironmentObject::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return Result;
}

TArray<class UStateEffect*> AEnvironmentObject::GetAllCurrentEffects()
{
	return CurrentEffects;
}

void AEnvironmentObject::RemoveEffect(UStateEffect* RemoveEffect)
{
	CurrentEffects.Remove(RemoveEffect);
}

void AEnvironmentObject::AddEffect(UStateEffect* NewEffect)
{
	CurrentEffects.Add(NewEffect);
}



FVector AEnvironmentObject::GetEffectPositionOffset()
{
	return FVector();
}

