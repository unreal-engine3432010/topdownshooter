// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryComponent.h"
#include "TopDownShooterGameInstance.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicatedByDefault(true);
}


// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	
	
}


// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UInventoryComponent::SwitchWeapon_OnServer_Implementation(int NewWeaponSlotIndex, bool ToNextWeapon, bool IsScrolling)
{
	
	if (!IsScrolling)
	{
		if (!WeaponSlots.IsValidIndex(NewWeaponSlotIndex))
		{
			return;
		}
		else if (WeaponSlots[NewWeaponSlotIndex].NameItem.IsNone())
		{
			return;
		}
	}

	if (NewWeaponSlotIndex >= WeaponSlots.Num())
	{
		NewWeaponSlotIndex = 0;
	}

	if (NewWeaponSlotIndex < 0)
	{
		NewWeaponSlotIndex = WeaponSlots.Num() - 1;
	}
	
	while (WeaponSlots[NewWeaponSlotIndex].NameItem.IsNone())
	{
		if (ToNextWeapon)
		{
			NewWeaponSlotIndex++;
		}
		else
		{
			NewWeaponSlotIndex--;
		}

		if (NewWeaponSlotIndex >= WeaponSlots.Num())
		{
			NewWeaponSlotIndex = 0;
		}

		if (NewWeaponSlotIndex < 0)
		{
			NewWeaponSlotIndex = WeaponSlots.Num() - 1;
		}
	}

	
	OnSwitchWeapon.Broadcast(WeaponSlots[NewWeaponSlotIndex].NameItem, WeaponSlots[NewWeaponSlotIndex].CurrentRounds, NewWeaponSlotIndex);
}

void UInventoryComponent::ChangeWeaponCurrentRounds(int WeaponIndex, int _CurrentRounds)
{
	WeaponSlots[WeaponIndex].CurrentRounds = _CurrentRounds;
	OnUpdateWeaponCurrentRounds.Broadcast(WeaponIndex, _CurrentRounds);
}

int UInventoryComponent::CheckAmmoForWeapon(EWeaponType WeaponType)
{
	for (int i = 0; i < AmmoSlots.Num(); i++)
	{
		if (AmmoSlots[i].WeaponType == WeaponType)
		{
			return AmmoSlots[i].Count;
		}
	}

	return 0;
}



bool UInventoryComponent::ChangeAmmoCount(EWeaponType WeaponType, int Ammo)
{
	for (int i = 0; i < AmmoSlots.Num(); i++)
	{
		if (AmmoSlots[i].WeaponType == WeaponType)
		{
			if (Ammo > 0)
			{
				if (AmmoSlots[i].Count == AmmoSlots[i].MaxCount)
				{
					return false;
				}
			}
			
			
				AmmoSlots[i].Count += Ammo;
				if (AmmoSlots[i].Count > AmmoSlots[i].MaxCount)
				{
					AmmoSlots[i].Count = AmmoSlots[i].MaxCount;
				}

				OnAmmoChange.Broadcast(i, AmmoSlots[i].Count);
				return true;
			
		}
	}

	return false;
}

void UInventoryComponent::TryTakeWeaponToInventory_OnServer_Implementation(AActor* PickupActor, FWeaponSlot WeaponSlot)
{
	for (int i = 0; i < WeaponSlots.Num(); i++)
	{
		if (WeaponSlots[i].NameItem.IsNone())
		{
			WeaponSlots[i] = WeaponSlot;
			OnUpdateWeaponSlot.Broadcast(i, WeaponSlot);
			if(PickupActor)
				PickupActor->Destroy();
		}
	}

}

void UInventoryComponent::UpdateWeaponSlot(int WeaponIndex, FWeaponSlot WeaponSlot)
{
	WeaponSlots[WeaponIndex] = WeaponSlot;
	OnUpdateWeaponSlot.Broadcast(WeaponIndex, WeaponSlot);
}

bool UInventoryComponent::GetWeaponSlotByIndex(int WeaponIndex, FWeaponSlot& WeaponSlot)
{
	if (WeaponSlots.IsValidIndex(WeaponIndex))
	{
		WeaponSlot = WeaponSlots[WeaponIndex];
		return true;
	}
	else
	{
		return false;
	}
}

void UInventoryComponent::InitInventory_OnServer_Implementation(const TArray<FWeaponSlot>& NewWeaponSlots, const TArray<FAmmoSlot>& NewAmmoSlots)
{
	WeaponSlots = NewWeaponSlots;
	AmmoSlots = NewAmmoSlots;

	for (int8 i = 0; i < WeaponSlots.Num(); i++)
	{
		UTopDownShooterGameInstance* myGI = Cast<UTopDownShooterGameInstance>(GetWorld()->GetGameInstance());
		if (myGI)
		{
			if (!WeaponSlots[i].NameItem.IsNone())
			{
				FWeaponInfo Info;
				if (myGI->GetWeaponInfoByName(WeaponSlots[i].NameItem, Info))
				{
					if(WeaponSlots[i].CurrentRounds > Info.MaxRound)
						WeaponSlots[i].CurrentRounds = Info.MaxRound;

				}
				
			}
		}
	}

	for (int8 i = 0; i < AmmoSlots.Num(); i++)
	{
		if(AmmoSlots[i].Count > AmmoSlots[i].MaxCount)
			AmmoSlots[i].Count = AmmoSlots[i].MaxCount;
	}

	

	if (WeaponSlots.IsValidIndex(0))
	{
		if (!WeaponSlots[0].NameItem.IsNone())
			OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].CurrentRounds, 0);
	}
}


void UInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UInventoryComponent, WeaponSlots);
	DOREPLIFETIME(UInventoryComponent, AmmoSlots);
}



