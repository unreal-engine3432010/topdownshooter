// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ProjectTypes.h"
#include "GameActor.h"
#include "TopDownShooterCharacter.generated.h"



UCLASS(Blueprintable)
class ATopDownShooterCharacter : public ACharacter, public IGameActor
{
	GENERATED_BODY()

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent = nullptr;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true"))
	class UInventoryComponent* InventoryComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
	class UCharacterHealthComponent* CharHealthComponent = nullptr;
	
	UFUNCTION()
	void InputAxisX(float Value);

	UFUNCTION()
	void InputAxisY(float Value);

	void MovementTick(float DeltaSeconds);

	float AxisX;

	float AxisY;

	void CursorTick();

	UDecalComponent* CurrentCursor = nullptr;

	UPROPERTY(Replicated)
	int CurrentWeaponSlotIndex = 0;

	void CharacterUpdate();

	void CharacterSprint(float DeltaSeconds);


	UFUNCTION()
	void InputFirePressed();

	UFUNCTION()
	void InputFireReleased();

	UFUNCTION()
	void InputNextWeapon();

	UFUNCTION()
	void InputPreviousWeapon();

	
	UFUNCTION()
	void WeaponReloadStart(float ReloadTime, UAnimMontage* Anim, float PlayRate);

	UFUNCTION()
	void WeaponReloadEnd(int RoundsTaken);

	UFUNCTION()
	void OnWeaponFire(UAnimMontage* AnimFireHip, UAnimMontage* AnimFireAim);

	UFUNCTION()
	void InputReloadPressed();

	UFUNCTION()
	void OnUpdateWeaponSlot(int WeaponIndex, FWeaponSlot WeaponSlot);

	UFUNCTION(Server, Reliable)
	void TryReloadWeapon_OnServer();

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION()
	void CharDead();


	void PauseAnims();

	FTimerHandle PauseAnimsTimer;
	
	UPROPERTY(Replicated)
	TArray<class UStateEffect*> CurrentStateEffects;

	UPROPERTY(ReplicatedUsing = EffectToAdd_OnRep)
	UStateEffect* EffectToAdd = nullptr;

	UPROPERTY(ReplicatedUsing = EffectToRemove_OnRep)
	UStateEffect* EffectToRemove = nullptr;

	UFUNCTION()
	void EffectToAdd_OnRep();

	UFUNCTION()
	void EffectToRemove_OnRep();

	void SwitchEffect(UStateEffect* _StateEffect, bool bAdd);

	//FName BoneName = FName("AttachEffectBone");

	UFUNCTION()
	void CheckAmmoForWeapon();

	UFUNCTION(Server, Unreliable)
	void SetActorRotationByYaw_OnServer(float Yaw);

	UFUNCTION(NetMulticast, Unreliable)
	void SetActorRotationByYaw_Multicast(float Yaw);

	UFUNCTION(Server, Reliable)
	void SetMovementState_OnServer(EMovementState _MovementState);

	UFUNCTION(NetMulticast, Reliable)
	void SetMovementState_Multicast(EMovementState _MovementState);

	virtual bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

	
	UFUNCTION(NetMulticast, Reliable)
	void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteEffect, FName _BoneName);

protected:

	UPROPERTY(BlueprintReadOnly)
	EMovementState MovementState = EMovementState::RUN_STATE;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FMovementSpeed MovementSpeed;

	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName IdWeapon, int CurrentRounds, int WeaponSlotIndex);


	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP(int WeaponIndex, float ReloadTime, UAnimMontage* Anim, float PlayRate);

	UFUNCTION(BlueprintNativeEvent)
	void OnWeaponFire_BP(UAnimMontage* Anim);

	UFUNCTION(BlueprintNativeEvent)
	void OnUpdateWeaponSlot_BP(int WeaponIndex, UTexture2D* WeaponIcon, int CurrentRounds, int MaxRounds, EWeaponType WeaponType);

	UFUNCTION(BlueprintNativeEvent)
	void OnUpdateWeaponCurrentRounds_BP(int WeaponIndex, int CurrentRounds);

	UFUNCTION(BlueprintNativeEvent)
	void OnAmmoChange_BP(int AmmoSlotIndex, int AmmoCount);

	UFUNCTION(BlueprintNativeEvent)
	void OnSwitchWeapon_BP(int WeaponIndex, int PreviousWeaponIndex, EWeaponType WeaponType);

	UFUNCTION(BlueprintNativeEvent)
	void OnNoAmmo_BP();

	UFUNCTION(BlueprintNativeEvent)
	void OnHaveAmmo_BP();

	UFUNCTION(BlueprintNativeEvent)
	void CharDead_BP();

	/*UPROPERTY(BlueprintReadWrite)
	bool bIsDead = false;*/

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	TArray<UAnimMontage*> DeadsAnim;

	UPROPERTY(Replicated, BlueprintReadWrite)
	AWeapon* CurrentWeapon;

	UFUNCTION(BlueprintCallable)
	void AttackCharEvent(bool bIsFiring);

	UFUNCTION(BlueprintNativeEvent)
	void DropItem_BP(FDropItem DropItemInfo, EWeaponType WeaponType);

public:

	ATopDownShooterCharacter();

	virtual void BeginPlay() override;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	FORCEINLINE class UCharacterHealthComponent* GetCharHealthComponent() const { return CharHealthComponent; }

	UPROPERTY(BlueprintReadWrite)
	bool WalkEnabled = false;

	UPROPERTY(BlueprintReadWrite)
	bool SprintEnabled = false;

	UPROPERTY(BlueprintReadWrite)
	bool Accelerate = false;

	UPROPERTY(BlueprintReadWrite)
	bool AimEnabled = false;

	UPROPERTY(BlueprintReadWrite)
	float Direction = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float Stamina = 100;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	UMaterialInterface* CursorMaterial = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);


	UPROPERTY(BlueprintReadOnly)
	float CurrentStamina = 0;

	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();

	UFUNCTION(BlueprintCallable)
	bool TryPickupAmmo(AActor* PickupActor, EWeaponType WeaponType, int AmmoCount);

	

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void TrySwitchWeaponToInventory_OnServer(AActor* PickupActor, int WeaponIndex, FDropItem PickupWeaponItem);

	
	UFUNCTION(BlueprintCallable)
	void SwitchWeapon(int WeaponIndex);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pawn")
	bool bIsPlayerPawn = true;

	EPhysicalSurface GetSurfaceType() override;

	TArray<class UStateEffect*> GetAllCurrentEffects() override;

	void RemoveEffect(UStateEffect* RemoveEffect) override;

	void AddEffect(UStateEffect* NewEffect) override;

	

	FVector GetEffectPositionOffset() override;

	/*UFUNCTION(BlueprintCallable)
	bool CharIsDead();*/
};

