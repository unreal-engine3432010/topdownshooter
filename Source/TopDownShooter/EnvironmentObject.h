// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameActor.h"
#include "EnvironmentObject.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API AEnvironmentObject : public AActor, public IGameActor
{
	GENERATED_BODY()
	
private:

	UPROPERTY()
	TArray<class UStateEffect*> CurrentEffects;

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	// Sets default values for this actor's properties
	AEnvironmentObject();

	// Called every frame
	virtual void Tick(float DeltaTime) override;


	EPhysicalSurface GetSurfaceType() override;


	TArray<class UStateEffect*> GetAllCurrentEffects() override;


	void RemoveEffect(UStateEffect* RemoveEffect) override;


	 void AddEffect(UStateEffect* NewEffect) override;

	 

	 FVector GetEffectPositionOffset() override;

};
