// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterHealthComponent.h"

float UCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;

	if (Shield > 100.0f)
		Shield = 100.0f;
	
	
	if (Shield < 0.0f)
		Shield = 0.0f;
	

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}

	ShieldChangeEvent_Multicast(Shield, ChangeValue);
	
}

void UCharacterHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UCharacterHealthComponent::RecoveryShield, ShieldRecoveryRate, true);
	}
}

void UCharacterHealthComponent::RecoveryShield()
{
	Shield += ShieldRecoveryValue;
	if (Shield > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}

	ShieldChangeEvent_Multicast(Shield, ShieldRecoveryValue);
}

void UCharacterHealthComponent::ShieldChangeEvent_Multicast_Implementation(float _Shield, float _ChangeValue)
{
	OnShieldChange.Broadcast(_Shield, _ChangeValue);
}

void UCharacterHealthComponent::ChangeHealthValue_OnServer_Implementation(float ChangeValue)
{
	
	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeValue *= CoefDamage;
		ChangeShieldValue(ChangeValue);

		if (Shield < 0.0f)
		{
			//FX
			//UE_LOG(LogTemp, Warning, TEXT("UTPSCharacterHealthComponent::ChangeHealthValue - Sheild < 0"));
		}
	}
	else
	{
		
		Super::ChangeHealthValue_OnServer_Implementation(ChangeValue);
	}
}
