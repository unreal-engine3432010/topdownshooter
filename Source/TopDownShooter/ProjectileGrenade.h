// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "ProjectileGrenade.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API AProjectileGrenade : public AProjectile
{
	GENERATED_BODY()



private:

	

	void ExplodeTick(float DeltaTime);

	float ExplodeTimer = 0;
	
	float ExplodeTime = 0;

	bool TimerEnabled = false;

	void Explode();

	UFUNCTION(NetMulticast, Reliable)
	void SpawnExplodeFX_Multicast(UParticleSystem* ExplodeFX);

	UFUNCTION(NetMulticast, Reliable)
	void SpawnExplodeSound_Multicast(USoundBase* ExplodeSound);

protected:

	void ImpactProjectile() override;

	UFUNCTION(Server, Reliable)
	void ImpactProjectile_OnServer();

public:

	virtual void Tick(float DeltaTime) override;

	AProjectileGrenade();
};
