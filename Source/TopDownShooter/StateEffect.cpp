// Fill out your copyright notice in the Description page of Project Settings.


#include "StateEffect.h"
#include "GameActor.h"
#include "Kismet/GameplayStatics.h"
#include "HealthComponent.h"
#include "TopDownShooterCharacter.h"
#include "Net/UnrealNetwork.h"


bool UStateEffect::InitObject(AActor* Actor, FName _BoneName)
{
	
	myActor = Actor;
	HitBoneName = _BoneName;

	IGameActor* myInterface = Cast<IGameActor>(myActor);
	
	if (myInterface)
	{
		myInterface->AddEffect(this);
		return true;
	}

	return false;
}


void UStateEffect::DestroyObject()
{
	IGameActor* myInterface = Cast<IGameActor>(myActor);

	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}

	myActor = nullptr;

	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
		
	}
}


bool UStateEffect_ExecuteTimer::InitObject(AActor* Actor, FName _BoneName)
{
	
	if(!Super::InitObject(Actor, _BoneName)) return false;

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UStateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UStateEffect_ExecuteTimer::Execute, RateTime, true);
	
	
	/*if (ParticleEffect)
	{
		
		if (ParticleEmitter == nullptr)
		{
			FName NameBoneToAttached;
			FVector Loc = FVector(0);

			if (BoneName.IsNone())
			{
				IGameActor* myInterface = Cast<IGameActor>(myActor);

				if (myInterface)
				{

					Loc = myInterface->GetEffectPositionOffset();
					ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				}
			}
			else
			{
				NameBoneToAttached = BoneName;
				USkeletalMeshComponent* MeshComponent = Actor->FindComponentByClass<USkeletalMeshComponent>();
				if(MeshComponent)
				    ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, MeshComponent, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			}

			
		}
	}*/

	return true;
}

void UStateEffect_ExecuteTimer::DestroyObject()
{
	UE_LOG(LogTemp, Warning, TEXT("Destroy Effect!!!"))

	GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	

	/*if (ParticleEmitter)
	{
		ParticleEmitter->DestroyComponent();
		ParticleEmitter = nullptr;

	}*/
	
	Super::DestroyObject();
}

void UStateEffect_ExecuteTimer::Execute()
{
	
	UE_LOG(LogTemp, Warning, TEXT("Execute, time remaining = %f"), GetWorld()->GetTimerManager().GetTimerRemaining(TimerHandle_EffectTimer))

	if (myActor)
	{
		
		UHealthComponent* myHealthComp = Cast<UHealthComponent>(myActor->GetComponentByClass(UHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			IGameActor* myInterface = Cast<IGameActor>(myActor);
			if (myInterface)
			{
				for (int i = 0; i < myInterface->GetAllCurrentEffects().Num(); i++)
				{
					if (myInterface->GetAllCurrentEffects()[i]->EffectName == FName("Invulnarability effect"))
					{
						return;
					}


				}
			}

			myHealthComp->ChangeHealthValue_OnServer(Power);

			/*ATopDownShooterCharacter* Character = Cast<ATopDownShooterCharacter>(myActor);
			if (Character)
			{
				if(!Character->CharIsDead())
					myHealthComp->ChangeHealthValue_OnServer(Power);
			}
			else
			{
				myHealthComp->ChangeHealthValue_OnServer(Power);
			}*/
			
		}
	}
}

bool UStateEffect_ExecuteOnce::InitObject(AActor* Actor, FName _BoneName)
{
	Super::InitObject(Actor, _BoneName);
	ExecuteOnce();
	return true;;
}

void UStateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UStateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UHealthComponent* myHealthComp = Cast<UHealthComponent>(myActor->GetComponentByClass(UHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue_OnServer(Power);
		}
	}

	DestroyObject();
}

void UStateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UStateEffect, HitBoneName);
	
}
