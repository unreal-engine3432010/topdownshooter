// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ProjectTypes.h"
#include "InventoryComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnSwitchWeapon, FName, WeaponIdName, int, WeaponCurrentRounds, int, NewCurrentIndexWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponSlot, int, WeaponSlotIndex, FWeaponSlot, WeaponSlot);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponCurrentRounds, int, WeaponSlotIndex, int, CurrentRounds);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChange, int, AmmoSlotIndex, int,  AmmoCount);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOPDOWNSHOOTER_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventoryComponent();

	FOnSwitchWeapon OnSwitchWeapon;

	FOnUpdateWeaponSlot OnUpdateWeaponSlot;

	FOnUpdateWeaponCurrentRounds OnUpdateWeaponCurrentRounds;

	FOnAmmoChange OnAmmoChange;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapons")
	TArray<FWeaponSlot> WeaponSlots;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapons")
	TArray<FAmmoSlot> AmmoSlots;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(Server, Reliable)
	void SwitchWeapon_OnServer(int NewWeaponSlotIndex, bool ToNextWeapon, bool IsScrolling);

	void ChangeWeaponCurrentRounds(int WeaponIndex, int CurrentRounds);

	int CheckAmmoForWeapon(EWeaponType WeaponType);

	
	bool ChangeAmmoCount(EWeaponType WeaponType, int Ammo);

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void TryTakeWeaponToInventory_OnServer(AActor* PickupActor, FWeaponSlot WeaponSlot);

	void UpdateWeaponSlot(int WeaponIndex, FWeaponSlot WeaponSlot);

	bool GetWeaponSlotByIndex(int WeaponIndex, FWeaponSlot& WeaponSlot);

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void InitInventory_OnServer(const TArray<FWeaponSlot>& NewWeaponSlots, const TArray<FAmmoSlot>& NewAmmoSlots);
};
