// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyCharacter.h"
#include "Particles/ParticleSystemComponent.h"
#include "StateEffect.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Engine/ActorChannel.h"
#include "ProjectileGrenade.h"
#include "Engine/DamageEvents.h"

// Sets default values
AEnemyCharacter::AEnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AEnemyCharacter::EffectToAdd_OnRep()
{
	SwitchEffect(EffectToAdd, true);
}

void AEnemyCharacter::EffectToRemove_OnRep()
{
	SwitchEffect(EffectToRemove, false);
}

void AEnemyCharacter::SwitchEffect(UStateEffect* _StateEffect, bool bAdd)
{
	if (_StateEffect && _StateEffect->ParticleEffect)
	{
		if (bAdd)
		{

			if (_StateEffect->ParticleEmitter == nullptr)
			{
				FName NameBoneToAttached;
				FVector Loc = FVector(0);

				if (_StateEffect->HitBoneName.IsNone())
				{

					Loc = GetEffectPositionOffset();
					_StateEffect->ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(_StateEffect->ParticleEffect, GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);

				}
				else
				{
					NameBoneToAttached = _StateEffect->HitBoneName;
					USkeletalMeshComponent* MeshComponent = GetMesh();
					if (MeshComponent)
						_StateEffect->ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(_StateEffect->ParticleEffect, MeshComponent, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				}


			}
		}
		else
		{
			_StateEffect->ParticleEmitter->DeactivateSystem();
			_StateEffect->ParticleEmitter->DestroyComponent();
			_StateEffect->ParticleEmitter = nullptr;
		}
	}
}

bool AEnemyCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (UStateEffect* StateEffect : CurrentStateEffects)
	{
		WroteSomething |= Channel->ReplicateSubobject(StateEffect, *Bunch, *RepFlags);
	}
	return WroteSomething;
}

void AEnemyCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteEffect, FName _BoneName)
{
	if (ExecuteEffect)
	{
		
		USkeletalMeshComponent* MeshComponent = GetMesh();
		if (MeshComponent)
			UGameplayStatics::SpawnEmitterAttached(ExecuteEffect, MeshComponent, _BoneName, FVector(0), FRotator::ZeroRotator, EAttachLocation::SnapToTarget, true);

	}
}

float AEnemyCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	AProjectileGrenade* Grenade = Cast<AProjectileGrenade>(DamageCauser);
	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID) && Grenade)
	{
		FuncLibrary::AddEffectBySurfaceType(this, Grenade->GetStateEffectClass(), GetSurfaceType(), FName(TEXT("Spine")));

	}

	return Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
}

// Called when the game starts or when spawned
void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

EPhysicalSurface AEnemyCharacter::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;

	if (GetMesh())
	{
		
		Result = GetMesh()->GetBodyInstance()->GetSimplePhysicalMaterial()->SurfaceType;
		
	}

	return Result;
}

TArray<class UStateEffect*> AEnemyCharacter::GetAllCurrentEffects()
{
	return CurrentStateEffects;
}

void AEnemyCharacter::RemoveEffect(UStateEffect* RemoveEffect)
{
	CurrentStateEffects.Remove(RemoveEffect);
	if (!RemoveEffect->bIsAutoDestroy)
	{
		EffectToRemove = RemoveEffect;
		SwitchEffect(EffectToRemove, false);
	}
}

void AEnemyCharacter::AddEffect(UStateEffect* NewEffect)
{
	CurrentStateEffects.Add(NewEffect);
	if (NewEffect->bIsAutoDestroy)
	{
		if (NewEffect->ParticleEffect)
			ExecuteEffectAdded_Multicast(NewEffect->ParticleEffect, NewEffect->HitBoneName);
	}
	else
	{
		EffectToAdd = NewEffect;
		SwitchEffect(EffectToAdd, true);
	}
}

FVector AEnemyCharacter::GetEffectPositionOffset()
{
	return FVector();
}

void AEnemyCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AEnemyCharacter, CurrentStateEffects);
	DOREPLIFETIME(AEnemyCharacter, EffectToAdd);
	DOREPLIFETIME(AEnemyCharacter, EffectToRemove);
}

