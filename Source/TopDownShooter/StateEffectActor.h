// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "StateEffect.h"
#include "StateEffectActor.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API AStateEffectActor : public AActor
{
	GENERATED_BODY()
	
private:
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USphereComponent* SphereComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UParticleSystemComponent* ParticleComponent = nullptr;

	
	UFUNCTION()
	void OverlapSphereEffect(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EffectSetting")
	TSubclassOf<UStateEffect> StateEffectClass = nullptr;

public:	

	// Sets default values for this actor's properties
	AStateEffectActor();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
