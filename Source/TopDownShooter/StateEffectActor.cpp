// Fill out your copyright notice in the Description page of Project Settings.


#include "StateEffectActor.h"
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "ProjectTypes.h"
#include "GameActor.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AStateEffectActor::AStateEffectActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
	RootComponent = SphereComponent;
	SphereComponent->bReturnMaterialOnMove = true;
	SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &AStateEffectActor::OverlapSphereEffect);

	ParticleComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleComponent"));
	ParticleComponent->SetupAttachment(RootComponent);
}

void AStateEffectActor::OverlapSphereEffect(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	
	IGameActor* GameActor = Cast<IGameActor>(OtherActor);
	if (StateEffectClass && GameActor)
	{
		FuncLibrary::AddEffectBySurfaceType(OtherActor, StateEffectClass, GameActor->GetSurfaceType(), FName());
	}
}

// Called when the game starts or when spawned
void AStateEffectActor::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void AStateEffectActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}



