// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectTypes.h"
#include "StateEffect.h"
#include "GameActor.h"


void FuncLibrary::AddEffectBySurfaceType(AActor* TakeEffectActor, TSubclassOf<UStateEffect> AddEffectClass, EPhysicalSurface SurfaceType, FName BoneName)
{
	if (SurfaceType == EPhysicalSurface::SurfaceType_Default || !TakeEffectActor || !AddEffectClass) return;
	bool bCanAddEffect = false;
	int index = 0;
	UStateEffect* MyEffect = AddEffectClass.GetDefaultObject();
	
	if (MyEffect && MyEffect->PossibleInteractSurface.Find(SurfaceType, index))
	{
		
		if (!MyEffect->bIsStakable)
		{
			
			IGameActor* GameActor = Cast<IGameActor>(TakeEffectActor);
			if (GameActor)
			{
				bCanAddEffect = true;
				for (int i = 0; i < GameActor->GetAllCurrentEffects().Num(); i++)
				{
					if (GameActor->GetAllCurrentEffects()[i]->EffectName == MyEffect->EffectName || GameActor->GetAllCurrentEffects()[i]->EffectName == FName("Invulnarability effect"))
					{
				       // UE_LOG(LogTemp, Warning, TEXT("Effect is already added or cannot be added!!!"))
							//GEngine->AddOnScreenDebugMessage(-1, 2, FColor::White, GameActor->GetAllCurrentEffects()[i]->EffectName.ToString());
						bCanAddEffect = false;
						break;
					}
					else
					{
						//UE_LOG(LogTemp, Warning, TEXT(""))
							
							//GEngine->AddOnScreenDebugMessage(-1, 2, FColor::Red, GameActor->GetAllCurrentEffects()[i]->EffectName.ToString());
					}
					
				}
			

			}

		}
		else
		{
			bCanAddEffect = true;
		}
	}

	if (bCanAddEffect)
	{
		
			UStateEffect* NewEffect = NewObject<UStateEffect>(TakeEffectActor, AddEffectClass);
		    
			if (NewEffect)
				NewEffect->InitObject(TakeEffectActor, BoneName);
		
	}
}
