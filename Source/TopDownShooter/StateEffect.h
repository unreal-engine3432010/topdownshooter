// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "StateEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class TOPDOWNSHOOTER_API UStateEffect : public UObject
{
	GENERATED_BODY()



protected:

	virtual void DestroyObject();

	AActor* myActor = nullptr;

	bool IsSupportedForNetworking() const override { return true; };

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	bool bIsStakable = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	FName EffectName = FName("Base effect");

	virtual bool InitObject(AActor* Actor, FName BoneName);
    
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	UParticleSystem* ParticleEffect = nullptr;

	UParticleSystemComponent* ParticleEmitter = nullptr;

	UPROPERTY(Replicated)
	FName HitBoneName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
	bool bIsAutoDestroy = false;
	
};

UCLASS()
class TOPDOWNSHOOTER_API UStateEffect_ExecuteOnce : public UStateEffect
{
	GENERATED_BODY()

private:
	virtual void ExecuteOnce();
	

protected:

	void DestroyObject() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
	float Power = 20.0f;

	

public:
	bool InitObject(AActor* Actor, FName BoneName) override;

	
};

UCLASS()
class TOPDOWNSHOOTER_API UStateEffect_ExecuteTimer : public UStateEffect
{
	GENERATED_BODY()

private:

	 void Execute();
	 
	 

	 FTimerHandle TimerHandle_ExecuteTimer = FTimerHandle();
	 FTimerHandle TimerHandle_EffectTimer;

	 

protected:

	void DestroyObject() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	float RateTime = 1.0f;

	

public:
	bool InitObject(AActor* Actor, FName BoneName) override;
	
	
};
