// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProjectTypes.h"
#include "Weapon.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnWeaponReloadStart, float, ReloadTime, UAnimMontage*, Anim, float, PlayRate);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadEnd, int, RoundsTaken);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponFire, UAnimMontage*, AnimFireHip, UAnimMontage*, AnimFireAim);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTryReloadWeapon);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnNoWeaponCurrentRounds);



UCLASS()
class TOPDOWNSHOOTER_API AWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeapon();

	FOnWeaponReloadStart OnWeaponReloadStart;

	FOnWeaponReloadEnd OnWeaponReloadEnd;

	FOnWeaponFire OnWeaponFire;

	FOnTryReloadWeapon OnTryReloadWeapon;

	FOnNoWeaponCurrentRounds OnNoWeaponCurrentRounds;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
     USceneComponent* SceneComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	 USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	 UStaticMeshComponent* StaticMeshWeapon = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* DropMagazineLocation = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* DropShellLocation = nullptr;

	FVector ShootEndLocation = FVector(0);

	FWeaponInfo WeaponSetting;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowDebug = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	float SizeVectorToChangeShootDirectionLogic = 100;

	int8 GetCurrentRounds();

	
	void InitReload(int Ammo);

	UFUNCTION(Server, Unreliable)
	void UpdateShootEndLocation_OnServer(FVector NewShootEndLocation);

private:

	void Fire();

	UPROPERTY(Replicated)
	int CurrentRounds = 0;

	float FireTimer = 0;

	float ReloadTimer = 0;

	bool bIsWeaponFiring = false;

	void FireTick(float DeltaTime);

	void ReloadTick(float DeltaTime);

	int RoundsForReloading = 0;

	void FinishReload();

	bool bIsWeaponReloading = false;

	FVector GetFireEndLocation();

	FVector ApplyDispersionToShoot(FVector DirectionShoot);

	float CurrentDispersion = 10;

	UFUNCTION(NetMulticast, Reliable)
	void DropMagazine_Multicast(FDropMeshInfo DropMeshInfo);

	void DropMagazineTick(float DeltaTime);

	bool DropMagazineFlag = false;

	float DropMagazineTimer = 0;
	
	void DispersionTick(float DeltaTime);

	bool ShouldReduceDispersion = false;

	float CurrentDispersionMin = 0;

	float CurrentDispersionMax = 0;

	float CurrentDispersionRecoil = 0;

	float CurrentDispersionReduction = 0;

	UFUNCTION(NetMulticast, Unreliable)
	void PlayWeaponAnim_Multicast(UAnimMontage* Anim, float PlayRate);

	UFUNCTION(NetMulticast, Unreliable)
	void DropBulletShell_Multicast(FDropMeshInfo DropMeshInfo);

	UFUNCTION(NetMulticast, Unreliable)
	void SpawnWeaponFireFX_Multicast(UParticleSystem* FireFX, USoundBase* FireSound);

	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitDecal_Multicast(UMaterialInterface* Decal, FHitResult HitResult);

	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitFX_Multicast(UParticleSystem* HitFX, FHitResult HitResult);

	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitSound_Multicast(USoundBase* HitSound, FHitResult HitResult);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;



public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void WeaponInit(FWeaponInfo WeaponInfo, int _CurrentRounds);

	UFUNCTION(Server, Reliable)
	void SetWeaponStateFire_OnServer(bool bIsFire);

	UFUNCTION(Server, Reliable)
	void UpdateStateWeapon_OnServer(EMovementState NewMovementState);

	

};
