// Fill out your copyright notice in the Description page of Project Settings.


#include "Magazine.h"

// Sets default values
AMagazine::AMagazine()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;
	
	StaticMeshMagazine = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshMagazine->SetupAttachment(RootComponent);
	StaticMeshMagazine->SetComponentTickEnabled(false);
	StaticMeshMagazine->SetGenerateOverlapEvents(false);
	StaticMeshMagazine->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
	StaticMeshMagazine->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	StaticMeshMagazine->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
	
	StaticMeshMagazine->SetSimulatePhysics(true);
	
	InitialLifeSpan = 5;

}

// Called when the game starts or when spawned
void AMagazine::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMagazine::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMagazine::SetMagazineMass(float Mass)
{
	StaticMeshMagazine->SetMassOverrideInKg("", Mass, true);
}



